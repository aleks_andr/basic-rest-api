// Router handling all calls to /transactions/* endpoint
import express from 'express';
import bodyParser from 'body-parser';
import { ObjectID } from 'mongodb';
import { getDb } from '../db';

const router = express.Router();

router.use(bodyParser.urlencoded({ extedned: true }));
router.use(bodyParser.json());

router.get("/:id?", async (req, res) => {
    const id = req.params.id;

    let searchTerm = {};

    if (id) {
        searchTerm = { _id: new ObjectID(id) };
    }

    const db = (await getDb()).db('apidata');
    const cursor = await db.collection("transactions").find(searchTerm);
    const docs = await cursor.toArray();

    if (docs.length === 0 ) {
        res.sendStatus(204);
        return;
    }

    res.json(docs);
});


// Inserts new "transaction" objects into the database
// Each object must have at least these properties:
// amount, timestamp, description
// Timestamp must be Unix time (in seconds)
router.post("/add", async (req, res) => {
    const payload = req.body;
    const db = (await getDb()).db('apidata');

    if (!payload || payload.constructor !== Array) {
        res.send(`Request body was incorrect: ${JSON.stringify(payload)}`, 400);
        return;
    }

    for (let item of payload) {
        if (!item.amount || !item.timestamp || !item.description || isNaN(item.amount)) {
            res.send("One or more supplied objects were invalid.", 400);
            return;
        }
    }

    const convertedObjects = payload.map(obj => ({...obj, timestamp: new Date(obj.timestamp * 1000)}));
    
    try {
        await db.collection("transactions").insertMany(convertedObjects);
        res.sendStatus(200);
    } catch (err) {
        console.error(err);
        res.sendStatus(500);
    }
});


// Removes transactions using a list of ObjectIds
router.post("/delete", async (req, res) => {
    const payload = req.body;

    // Payload should be of form:
    // { ids: Array }
    if (!payload || payload.constructor !== Array) {
        res.send("Invalid request body.", 400);
        return;
    }
    // Map list of IDs to ObjectId instances
    const objectIds = payload.map(id => new ObjectID(id));

    const db = (await getDb()).db('apidata');

    try {
        await db.collection("transactions").deleteMany({
            _id: { $in: objectIds }
        });
        res.sendStatus(200);
    } catch (err) {
        console.error(err);
        res.sendStatus(500);
    }
});

// Calculate the net gain/loss across a set time span
// :starttime is a required parameter
// :endtime is optional, current time is used by default
// Times are in Unix timestamp form
router.get("/diff/:starttime/:endtime?", async (req, res) => {
    const start = new Date (req.params.starttime * 1000);
    const end = req.params.endtime ? new Date(req.params.endtime * 1000) : new Date();
    
    const db = (await getDb()).db('apidata');

    try {
        const cursor = await db.collection("transactions").find({
            timestamp: {
                    $gte: start,
                    $lte: end
            }
        });

        const docs = await cursor.toArray();
        
        const delta = docs.map(doc => doc.amount).reduce((a, b) => a + b, 0);

        res.json({
            transactions: docs.length,
            delta: delta,
            startTime: start,
            endTime: end
        });

    } catch (err) {
        console.error(err);
        res.sendStatus(500);
    }
});

export default router;
