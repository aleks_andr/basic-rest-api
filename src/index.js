import express from 'express';
import bodyParser from 'body-parser';

import { getDb } from './db';
import transactionRouter from './routers/transactions';

const app = express();

// Attach middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Define routers to use
app.use("/transactions", transactionRouter);

app.get("/", (req, res) => {
    res.send("OK");
});

getDb().then(() => {
    app.listen(3000, () => console.log("Listening on port 3000"));
}).catch((err) => {
    console.error(err);
});
