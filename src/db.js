import mongodb from 'mongodb';

const DB_URL = "mongodb://db";

const client = mongodb.MongoClient;
let dbPromise = null;

const getDb = async () => dbPromise || (dbPromise = client.connect(DB_URL), { useNewUrlParser: true });

export { getDb }
