

# About

This repository contains a simple REST API implemented in NodeJS
using ExpressJS with MongoDB as a database system. It was initially designed
to be used a backend for a personal finance tracking app, thus its functionality is limited
to storing, handling and serving "transaction" events.

In the future, user management could be implemented.

Demo video link: <https://drive.google.com/file/d/1UwH5REXHL61MWaIQBcUkTetsxCPDSJAu/view>
Made by: Andrey Aleksandrov, 0447608


# Usage


## Dependencies

-   Docker
-   Docker-compose


## Starting the API

This project uses `docker-compose` to orchestrate the different
Docker containers required.  Thus to run the API, you first need
to build the images with:

    docker-compose build 

After building the images, containers may be started with:

    docker-compose up 

